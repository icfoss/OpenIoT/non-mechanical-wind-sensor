# Non-Mechanical Wind Sensor


Currently,weather elements wind is monitored using mechanical instruments like cup anemometer which require regular maintenence .
This project aims at producing locally maintainable  weather stations at cheaper cost ,low power capabilities while ensuring accurate and dependable 
results using Machine Learcning(ML) at the Edge.
![Non mechanical Wind sensor-PoC](https://gitlab.com/diyaupradeep/non-mechanical-wind-sensor/-/raw/main/Images%20&%20Videos/wind.png)


# List of Parameters recorded
# WIND
- Ultrasonic measurement


# Prerequisites
  - Arduino IDE [Tested]
  - Edge Impulse Software [Tested]

# Getting Started
  # WIND
  - Connect the Components as shown in the [Wiring Diagram](https://gitlab.com/diyaupradeep/non-mechanical-wind-sensor/-/blob/main/Hardware/circuit_dia.pdf)
